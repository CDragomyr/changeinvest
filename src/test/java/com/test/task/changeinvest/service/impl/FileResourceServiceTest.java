package com.test.task.changeinvest.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.task.changeinvest.model.StarSystem;
import com.test.task.changeinvest.service.CalculatorService;

class FileResourceServiceTest {

    private final CalculatorService fakeCalculatorService = new FakeCalculatorService();

    private StarSystem starSystem;

    @BeforeEach
    void beforeEach() throws IOException {
        var classLoader = getClass().getClassLoader();
        var file = new File(classLoader.getResource("solar-system.json").getFile());
        var objectMapper = new ObjectMapper();
        var fileResourceService = new FileResourceService(objectMapper);
        starSystem = fileResourceService.uploadDataFromSource(file.getAbsolutePath());
    }

    private static Stream<Arguments> data() {
        return Stream.of(
                Arguments.of("Moon", "earth", 384_400L),
                Arguments.of("Earth", "Venus", 42_000_000L),
                Arguments.of("moon", "sun", 150_384_400L),
                Arguments.of("Earth", "Sun", 150_000_000L),
                Arguments.of("Phobos", "Moon", 155_405_600L)

        );
    }

    @MethodSource("data")
    @ParameterizedTest
    void uploadDataFromSource(String from, String to, Long expectedDistance) {
        var distance = fakeCalculatorService.calculateDistance(from, to, starSystem);
        assertThat(distance).isEqualTo(expectedDistance);
    }
}