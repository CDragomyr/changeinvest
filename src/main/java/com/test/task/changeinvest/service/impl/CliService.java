package com.test.task.changeinvest.service.impl;

import java.io.IOException;

import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import org.springframework.shell.standard.ShellOption;

import com.test.task.changeinvest.model.StarSystem;
import com.test.task.changeinvest.service.CalculatorService;
import com.test.task.changeinvest.service.ResourceService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ShellComponent
@RequiredArgsConstructor
public class CliService {

    private final ResourceService cliResourceService;
    private final CalculatorService fakeCalculatorService;

    private StarSystem starSystem;

    @ShellMethod(value = "Import data from file", key = {"import", "i"})
    public String importData(@ShellOption(defaultValue = "solar-system.json") String pathToFile) {
        try {
            starSystem = cliResourceService.uploadDataFromSource(pathToFile);
            return "OK";
        } catch (IOException e) {
            log.error("File [{}] cannot be parsed", pathToFile, e);
            return "ERROR";
        }
    }

    @ShellMethod(value = "Calculate distance", key = {"c", "calc"})
    public Long calculate(@ShellOption(defaultValue = "Moon") String from,
                          @ShellOption(defaultValue = "Earth") String to) {
        return fakeCalculatorService.calculateDistance(from, to, starSystem);
    }

    @ShellMethodAvailability("calculate")
    public Availability calculateAvailability() {
        return starSystem != null ? Availability.available() : Availability.unavailable("you are not imported data file");
    }
}
