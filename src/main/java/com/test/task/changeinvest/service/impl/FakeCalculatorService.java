package com.test.task.changeinvest.service.impl;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.test.task.changeinvest.model.Planet;
import com.test.task.changeinvest.model.Satellite;
import com.test.task.changeinvest.model.StarSystem;
import com.test.task.changeinvest.model.matchedspaceobject.MatchedPlanet;
import com.test.task.changeinvest.model.matchedspaceobject.MatchedSatellite;
import com.test.task.changeinvest.model.matchedspaceobject.MatchedStarSystem;
import com.test.task.changeinvest.service.CalculatorService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FakeCalculatorService implements CalculatorService {

    private static Long getDistanceToSpaceObject(MatchedStarSystem starSystem) {
        long distanceToSpaceObject = starSystem.hasPlanet() ? starSystem.getPlanet().getRadiusOfOrbit() : 0;
        if (starSystem.hasPlanet() && starSystem.getPlanet().hasSatellite()) {
            distanceToSpaceObject += starSystem.getPlanet().getSatellite().getRadiusOfOrbit();
        }

        return distanceToSpaceObject;
    }

    private static void validateMatchedSpaceObjects(final MatchedStarSystem from, final MatchedStarSystem to) {
        if (Objects.equals(from, to)) {
            log.warn("Matched space objects are the same. From - [{}]; to - [{}]", from, to);
        }
    }

    private static void validateMatchedSpaceObject(String spaceObjectName, MatchedStarSystem matchedStarSystem) {
        if (matchedStarSystem == null || StringUtils.isBlank(matchedStarSystem.getName())) {
            var message = String.format("Space object with %s name not found", spaceObjectName);
            log.error(message);
            throw new IllegalArgumentException(message);
        }
    }

    private static MatchedStarSystem matchSpaceObject(final String spaceObjectName, final StarSystem starSystem) {
        var matchedStarSystem = new MatchedStarSystem();
        matchedStarSystem.setName(starSystem.getName());

        if (StringUtils.equalsIgnoreCase(spaceObjectName, starSystem.getName())) {
            return matchedStarSystem;
        }

        return matchSpaceObject(matchedStarSystem, spaceObjectName, starSystem.getPlanets());
    }

    private static MatchedStarSystem matchSpaceObject(final MatchedStarSystem matchedStarSystem, final String spaceObjectName, final Set<Planet> planets) {
        MatchedPlanet matchedPlanet = null;
        for (var planet : planets) {
            if (StringUtils.equalsIgnoreCase(spaceObjectName, planet.getName())) {
                matchedPlanet = buildMatchedPlanet(planet);
            } else if (CollectionUtils.isNotEmpty(planet.getSatellites())) {
                var optionalMatchedSatellite = matchSatellite(spaceObjectName, planet.getSatellites());
                if (optionalMatchedSatellite.isPresent()) {
                    matchedPlanet = buildMatchedPlanet(planet);
                    matchedPlanet.setSatellite(optionalMatchedSatellite.get());
                }
            }

            if (matchedPlanet != null) {
                matchedStarSystem.setPlanet(matchedPlanet);
                break;
            }
        }

        return matchedStarSystem;
    }

    private static MatchedPlanet buildMatchedPlanet(final Planet planet) {
        MatchedPlanet matchedPlanet = new MatchedPlanet();
        matchedPlanet.setName(planet.getName());
        matchedPlanet.setRadiusOfOrbit(planet.getRadiusOfOrbit());

        return matchedPlanet;
    }

    private static Optional<MatchedSatellite> matchSatellite(final String satelliteName, final Set<Satellite> satellites) {
        return satellites.stream()
                         .filter(satellite -> StringUtils.equalsIgnoreCase(satelliteName, satellite.getName()))
                         .map(satellite -> new MatchedSatellite(satellite.getName(), satellite.getRadiusOfOrbit()))
                         .findFirst();
    }

    @Override
    public Long calculateDistance(final String fromSpaceObject, final String toSpaceObject, final StarSystem starSystem) {
        var from = matchSpaceObject(fromSpaceObject, starSystem);
        validateMatchedSpaceObject(fromSpaceObject, from);

        var to = matchSpaceObject(toSpaceObject, starSystem);
        validateMatchedSpaceObject(toSpaceObject, to);

        validateMatchedSpaceObjects(from, to);

        var distanceFrom = getDistanceToSpaceObject(from);
        var distanceTo = getDistanceToSpaceObject(to);

        var maxDistance = Math.max(distanceFrom, distanceTo);
        var minDistance = Math.min(distanceFrom, distanceTo);

        var distance = maxDistance - minDistance;
        log.info("Distance between {} and {} is {}", fromSpaceObject, toSpaceObject, distance);

        return distance;
    }
}
