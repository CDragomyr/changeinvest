package com.test.task.changeinvest.service.impl;

import java.io.FileInputStream;
import java.io.IOException;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.task.changeinvest.model.StarSystem;
import com.test.task.changeinvest.service.ResourceService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FileResourceService implements ResourceService {

    private final ObjectMapper objectMapper;

    @Override
    public StarSystem uploadDataFromSource(final Object source) throws IOException {
        try {
            return objectMapper.readValue(new FileInputStream((String) source), StarSystem.class);
        } catch (IOException e) {
            throw new IOException(e);
        }
    }
}
