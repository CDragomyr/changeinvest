package com.test.task.changeinvest.service;

import com.test.task.changeinvest.model.StarSystem;

public interface CalculatorService {

    Long calculateDistance(String fromSpaceObject, String toSpaceObject, StarSystem starSystem);
}
