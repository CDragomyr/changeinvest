package com.test.task.changeinvest.service;

import java.io.IOException;

import com.test.task.changeinvest.model.StarSystem;

public interface ResourceService {

    StarSystem uploadDataFromSource(Object source) throws IOException;
}
