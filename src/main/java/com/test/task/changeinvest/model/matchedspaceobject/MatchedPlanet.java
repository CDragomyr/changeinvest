package com.test.task.changeinvest.model.matchedspaceobject;

import org.apache.commons.lang3.builder.ToStringBuilder;

import lombok.Data;

@Data
public class MatchedPlanet {

    private String name;
    private Long radiusOfOrbit;
    private MatchedSatellite satellite;

    public boolean hasSatellite() {
        return satellite != null;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
