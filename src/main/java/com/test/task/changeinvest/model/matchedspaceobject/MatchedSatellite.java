package com.test.task.changeinvest.model.matchedspaceobject;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MatchedSatellite {

    private String name;
    private Long radiusOfOrbit;
}
