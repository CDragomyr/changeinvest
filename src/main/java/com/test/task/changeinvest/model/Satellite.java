package com.test.task.changeinvest.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Satellite implements SpaceObject, Comparable<Satellite> {

    private String name;
    @JsonProperty("radius-of-orbit")
    private Long radiusOfOrbit;

    @Override
    public int compareTo(final Satellite o) {
        return o.getRadiusOfOrbit().compareTo(getRadiusOfOrbit()) * -1;
    }
}
