package com.test.task.changeinvest.model;

public interface SpaceObject {

    String getName();

    default Long getRadiusOfOrbit() {
        return 0L;
    }
}
