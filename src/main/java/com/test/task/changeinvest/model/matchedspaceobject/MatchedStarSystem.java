package com.test.task.changeinvest.model.matchedspaceobject;

import org.apache.commons.lang3.builder.ToStringBuilder;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class MatchedStarSystem {

    private String name;
    private MatchedPlanet planet;

    public boolean hasPlanet() {
        return planet != null;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
