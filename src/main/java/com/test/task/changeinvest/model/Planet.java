package com.test.task.changeinvest.model;

import java.util.Set;
import java.util.TreeSet;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Planet implements SpaceObject, Comparable<Planet> {

    private String name;
    @JsonProperty("radius-of-orbit")
    private Long radiusOfOrbit;
    private TreeSet<Satellite> satellites;

    @Override
    public int compareTo(Planet o) {
        return o.getRadiusOfOrbit().compareTo(getRadiusOfOrbit()) * -1;
    }
}
