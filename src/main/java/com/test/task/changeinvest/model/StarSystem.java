package com.test.task.changeinvest.model;

import java.util.Set;
import java.util.TreeSet;

import lombok.Data;

@Data
public class StarSystem implements SpaceObject {

    private String name;
    private TreeSet<Planet> planets;
}
