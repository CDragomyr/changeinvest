## Simple app to get distance between space objects.

### Small description

Following interfaces could be implemented in order to get own implementation:
- `SpaceObject` - represents a space object. Should be set `name` and radius of orbit (default value is 0 - meaning the star of the solar system)
- `ResourceService` - resource of the data to be processed. Implemented by `FileResourceService` to get data from JSON file
- `CalculatorService` - calculate distance between space objects. Implemented by `FakeCalculatorService`

### How to start

`$ ./mvnw clean spring-boot:run`

### Shell commands:

- `i` (or `import`) - import json file (sample file is provided - `solar-system.json`). Example of command: `$ i solar-system.json`
- `c` (or `calc`) - get distance between space objects. Example of command: `$ c moon venus`